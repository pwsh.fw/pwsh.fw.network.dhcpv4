# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-06-23

### Added

-	constructor to build a DHCPv4 packet
-	methods to convert packet into array of int, bytes and hexadecimal string
-	methods to send and receive packets over the network
