
[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

# PwSh.Fw.Network.DHCPv4

This is a powershell implementation of the DHCPv4 protocol as described in [RFC 2131](https://www.ietf.org/rfc/rfc2131.txt) and [RFC 2132](https://www.ietf.org/rfc/rfc2132.txt)

The DHCP packet and its options are declared in `DHCPv4.class.ps1` file.

The file `DHCPv4.enum.ps1` contains several enums and hastables :
-	`DHCPv4PacketStructure` : hastable describing a DHCP packet structure
-	`DHCPv4MessageType` : enum of all message types available
-	`PAD`, `END` and `DHCP_MAGIC_COOKIE` : constants with their values according to RFC.
-	`DHCPv4OptionCode` : enum of all option codes
-	`hDHCPv4Options` : hashtable of options. It contains the code, label, type, length and sometimes default value of each DHCP option
-	`aDHCPv4Options` : same as above, but in an array where each cell is the option code.

2 test scripts are in the `tests` folder
-	`send-dhcp.ps1` : send a DHCP discovery packet over the network. You have to provide the mac address of your machine.
-	`receive-dhcp.ps1` : listen to network for a DHCP offer.
These test scripts are to be used together. Open 2 powershell terminals. In the first one, enter

```powershell
./receive-dhcp.ps1
```

and in the second one

```powershell
./send-dhcp.ps1 -macAddress "aa:bb:cc:dd:ee:ff"
```

Check the terminal #1 and you'll see DHCP offers coming.
