Write-Devel "Enum PSScriptRoot = $PSScriptRoot"

# https://www.ietf.org/rfc/rfc2131.txt
$Global:DHCPv4PacketStructure = [ordered]@{
	# code = length (in bytes)
	op = 1
	htype = 1
	hlen = 1
	hops = 1
	xid = 4
	flags = 2
	ciaddr = 4
	yiaddr = 4
	siaddr = 4
	giaddr = 4
	chaddr = 16
	sname = 64
	file = 128
}

# https://www.ietf.org/rfc/rfc2132.txt
# $DHCPv4OptionStructure = [ordered]@{
# 	# code = name, length, type, help
# 	# @see @url http://www.ietf.org/rfc/rfc2132.txt §3.1
# 	0 = @('Pad', 1, "int", "The pad option can be used to cause subsequent fields to align on word boundaries.")
# 	# @see @url http://www.ietf.org/rfc/rfc2132.txt §3.3
# 	1 = @('SubnetMask', 4, 'ipaddress', "The subnet mask option specifies the client's subnet mask as per RFC 950 [5].")
# 	# @see @url http://www.ietf.org/rfc/rfc2132.txt §3.4
# 	2 = @('TimeOffset', 4, 'int', "The time offset field specifies the offset of the client's subnet in seconds from Coordinated Universal Time (UTC). The offset is expressed as a two's complement 32-bit integer. A positive offset indicates a location east of the zero meridian and a negative offset indicates a location west of the zero meridian.")
# 	# @see @url http://www.ietf.org/rfc/rfc2132.txt §3.5
# 	3 = @('Router', 4, 'ipaddress', "The router option specifies a list of IP addresses for routers on the client's subnet.  Routers SHOULD be listed in order of preference.")
# 	# @see @url http://www.ietf.org/rfc/rfc2132.txt §3.6
# 	4 = @('TimeServer', 4, 'ipaddress', "The time server option specifies a list of RFC 868 [6] time servers available to the client.  Servers SHOULD be listed in order of preference.")
# 	# @see @url http://www.ietf.org/rfc/rfc2132.txt §3.2
# 	255 = @('End', 1, "int", "The end option marks the end of valid information in the vendor field.  Subsequent octets should be filled with pad options.")
# }

# https://www.ietf.org/rfc/rfc2132.txt
# 9.6. DHCP Message Type
enum DHCPv4MessageType {
    DHCPDISCOVER = 1
    DHCPOFFER = 2
    DHCPREQUEST = 3
    DHCPDECLINE = 4
    DHCPACK = 5
    DHCPNAK = 6
    DHCPRELEASE = 7
    DHCPINFORM = 8
}

# https://www.ietf.org/rfc/rfc2132.txt
# 3.1. Pad Option
$Global:PAD = 0

# https://www.ietf.org/rfc/rfc2132.txt
# 3.2. End Option
$Global:END = 255

# @url http://www.ietf.org/rfc/rfc2131.txt §3 [Page 13]
$Global:DHCP_MAGIC_COOKIE = @(99,130,83,99)

# # number of options
# # @see @url http://www.ietf.org/rfc/rfc2132.txt
enum DHCPv4OptionCode {
	PAD = 0
# RFC section 3
	SubnetMask = 1
	TimeOffset = 2
	Router = 3
	TimeServer = 4
	NameServer = 5
	DomainNameServer = 6
	LogServer = 7
	CookieServer = 8
	LPRServer = 9
	ImpressServer = 10
	ResourceLocationServer = 11
	Hostname = 12
	BootFileSize = 13
	MeritDump = 14
	DomainName = 15
	SwapServer = 16
	RootPath = 17
	ExtensionsPath = 18
# RFC section 4
	IPForwarding = 19
	NonLocalSourceRouting = 20
	PolicyFilter = 21
	MaximumDatagramReassemblySize = 22
	DefaultIPTTL = 23
	PathMTUAgingTimeout = 24
	PathMTUPlateauTable = 25
# RFC section 5
	InterfaceMTU = 26
	AllSubnetsareLocal = 27
	BroadcastAddress = 28
	PerformMaskDiscovery = 29
	MaskSupplier = 30
	PerformRouteriscovery = 31
	RouterSolicitationddress = 32
	StaticRoute = 33
# RFC section 6
	TrailerEncapsulation = 34
	ARPCacheimeout = 35
	EthernetEncapsulation = 36
# RFC section 7
	TCPDefaultTL = 37
	TCPKeepaliventerval = 38
	TCPKeepalivearbage = 39
# RFC section 8
	NISDomain = 40
	NISServer = 41
	NTPServer = 42
	VendorSpecificInformation = 43
	NetBIOSNameServer = 44
	NetBIOSDatagramDistributionServer = 45
	NetBIOSNodeType = 46
	NetBIOSScope = 47
	XFontServer = 48
	XDisplayManager = 49
	NISPLUSDomain = 64
	NISPLUSServer = 65
	MobileIPHomeAgent = 68
	SMTPServer = 69
	POP3Server = 70
	NNTPServer = 71
	WWWServer = 72
	FingerServer = 73
	IRCServer = 74
	StreetTalkServer = 75
	STDAServer = 76
# RFC section 9
	RequestedIPAddress = 50
	IPAddressLeaseTime = 51
	OptionOverload = 52
	DHCPMessageType = 53
	ServerIdentifier = 54
	ParameterRequestList = 55
	Message = 56
	MaximumDHCPMessageSize = 57
	RenewalTimeValue = 58
	RebindingTimeValue = 59
	Vendorclassidentifier = 60
	ClientIdentifier = 61
	TFTPservername = 66
	Bootfilename = 67
#
	END = 255
}

# DHCPv4 options listed in a hashtable
# @see @url http://www.ietf.org/rfc/rfc2132.txt
$Global:hDHCPv4Options = @{
	'PAD' = @{							'code' = 0;		'type' = 'none';		'length' = 1; 'label' = "PAD";	"value" = 0 }
# RFC section 3
	'SubnetMask' = @{					'code' = 1;		'type' = 'ip';			'length' = 4; 'label' = "SubnetMask" }
	'TimeOffset' = @{					'code' = 2;		'type' = 'int';			'length' = 4; 'label' = "TimeOffset" }
	'Router' = @{						'code' = 3;		'type' = 'ip-array';	'length' = 0; 'label' = "Router" }
	'TimeServer' = @{					'code' = 4;		'type' = 'ip-array';	'length' = 0; 'label' = "TimeServer" }
	'NameServer' = @{					'code' = 5;		'type' = 'ip-array';	'length' = 0; 'label' = "NameServer" }
	'DomainNameServer' = @{				'code' = 6;		'type' = 'ip-array';	'length' = 0; 'label' = "DomainNameServer" }
	'LogServer' = @{					'code' = 7;		'type' = 'ip-array';	'length' = 0; 'label' = "LogServer" }
	'CookieServer' = @{					'code' = 8;		'type' = 'ip-array';	'length' = 0; 'label' = "CookieServer" }
	'LPRServer' = @{					'code' = 9;		'type' = 'ip-array';	'length' = 0; 'label' = "LPRServer" }
	'ImpressServer' = @{				'code' = 10;	'type' = 'ip-array';	'length' = 0; 'label' = "ImpressServer" }
	'ResourceLocationServer' = @{		'code' = 11;	'type' = 'ip-array';	'length' = 0; 'label' = "ResourceLocationServer" }
	'Hostname' = @{						'code' = 12;	'type' = 'string';		'length' = 0; 'label' = "Hostname" }
	'BootFileSize' = @{					'code' = 13;	'type' = 'int';			'length' = 2; 'label' = "BootFileSize" }
	'MeritDump' = @{					'code' = 14;	'type' = 'string';		'length' = 0; 'label' = "MeritDump" }
	'DomainName' = @{					'code' = 15;	'type' = 'string';		'length' = 0; 'label' = "DomainName" }
	'SwapServer' = @{					'code' = 16;	'type' = 'ip';			'length' = 4; 'label' = "SwapServer" }
	'RootPath' = @{						'code' = 17;	'type' = 'string';		'length' = 0; 'label' = "RootPath" }
	'ExtensionsPath' = @{				'code' = 18;	'type' = 'string';		'length' = 0; 'label' = "ExtensionsPath" }
# RFC section 4
	'IPForwarding' = @{					'code' = 19;	'type' = 'bool';		'length' = 1; 'label' = "IPForwarding" }
	'NonLocalSourceRouting' = @{		'code' = 20;	'type' = 'bool';		'length' = 1; 'label' = "NonLocalSourceRouting" }
	'PolicyFilter' = @{					'code' = 21;	'type' = 'ip-array';	'length' = 0; 'label' = "PolicyFilter" }
	'MaximumDatagramReassemblySize' = @{'code' = 22;	'type' = 'int';			'length' = 2; 'label' = "MaximumDatagramReassemblySize" }
	'DefaultIPTTL' = @{					'code' = 23;	'type' = 'int';			'length' = 1; 'label' = "DefaultIPTTL" }
	'PathMTUAgingTimeout' = @{			'code' = 24;	'type' = 'int';			'length' = 4; 'label' = "PathMTUAgingTimeout" }
	'PathMTUPlateauTable' = @{			'code' = 25;	'type' = 'int-array';	'length' = 0; 'label' = "PathMTUPlateauTable" }
# RFC section 5
	'InterfaceMTU' = @{					'code' = 26;	'type' = 'int';			'length' = 2; 'label' = "InterfaceMTU" }
	'AllSubnetsareLocal' = @{			'code' = 27;	'type' = 'bool';		'length' = 1; 'label' = "AllSubnetsareLocal" }
	'BroadcastAddress' = @{				'code' = 28;	'type' = 'ip';			'length' = 4; 'label' = "BroadcastAddress" }
	'PerformMaskDiscovery' = @{			'code' = 29;	'type' = 'bool';		'length' = 1; 'label' = "PerformMaskDiscovery" }
	'MaskSupplier' = @{					'code' = 30;	'type' = 'bool';		'length' = 1; 'label' = "MaskSupplier" }
	'PerformRouteriscovery' = @{		'code' = 31;	'type' = 'bool';		'length' = 1; 'label' = "PerformRouteriscovery" }
	'RouterSolicitationddress' = @{		'code' = 32;	'type' = 'ip';			'length' = 4; 'label' = "RouterSolicitationddress" }
	'StaticRoute' = @{					'code' = 33;	'type' = 'ip-array';	'length' = 0; 'label' = "StaticRoute" }
# RFC section 6
	'TrailerEncapsulation' = @{			'code' = 34;	'type' = 'bool';		'length' = 1; 'label' = "TrailerEncapsulation" }
	'ARPCacheimeout' = @{				'code' = 35;	'type' = 'int';			'length' = 4; 'label' = "ARPCacheimeout" }
	'EthernetEncapsulation' = @{		'code' = 36;	'type' = 'bool';		'length' = 1; 'label' = "EthernetEncapsulation" }
# RFC section 7
	'TCPDefaultTL' = @{					'code' = 37;	'type' = 'int';			'length' = 1; 'label' = "TCPDefaultTL" }
	'TCPKeepaliventerval' = @{			'code' = 38;	'type' = 'int';			'length' = 4; 'label' = "TCPKeepaliventerval" }
	'TCPKeepalivearbage' = @{			'code' = 39;	'type' = 'bool';		'length' = 1; 'label' = "TCPKeepalivearbage" }
# RFC section 8
	'NISDomain' = @{					'code' = 40;	'type' = 'string';		'length' = 0; 'label' = "NISDomain" }
	'NISServer' = @{					'code' = 41;	'type' = 'ip-array';	'length' = 0; 'label' = "NISServer" }
	'NTPServer' = @{					'code' = 42;	'type' = 'ip-array';	'length' = 0; 'label' = "NTPServer" }
	'VendorSpecificInformation' = @{	'code' = 43;	'type' = 'raw';			'length' = 0; 'label' = "VendorSpecificInformation" }
	'NetBIOSNameServer' = @{			'code' = 44;	'type' = 'ip-array';	'length' = 0; 'label' = "NetBIOSNameServer" }
	'NetBIOSDatagramDistributionServer' = @{'code' = 45;'type' = 'ip-array';	'length' = 0; 'label' = "NetBIOSDatagramDistributionServer" }
	'NetBIOSNodeType' = @{				'code' = 46;	'type' = 'enum';		'length' = 1; 'label' = "NetBIOSNodeType" }
	'NetBIOSScope' = @{					'code' = 47;	'type' = 'string';		'length' = 0; 'label' = "NetBIOSScope" }
	'XFontServer' = @{					'code' = 48;	'type' = 'ip-array';	'length' = 0; 'label' = "XFontServer" }
	'XDisplayManager' = @{				'code' = 49;	'type' = 'ip-array';	'length' = 0; 'label' = "XDisplayManager" }
	'NISPLUSDomain' = @{				'code' = 64;	'type' = 'string';		'length' = 0; 'label' = "NISPLUSDomain" }
	'NISPLUSServer' = @{				'code' = 65;	'type' = 'ip-array';	'length' = 0; 'label' = "NISPLUSServer" }
	'MobileIPHomeAgent' = @{			'code' = 68;	'type' = 'ip-array';	'length' = 0; 'label' = "MobileIPHomeAgent" }
	'SMTPServer' = @{					'code' = 69;	'type' = 'ip-array';	'length' = 0; 'label' = "SMTPServer" }
	'POP3Server' = @{					'code' = 70;	'type' = 'ip-array';	'length' = 0; 'label' = "POP3Server" }
	'NNTPServer' = @{					'code' = 71;	'type' = 'ip-array';	'length' = 0; 'label' = "NNTPServer" }
	'WWWServer' = @{					'code' = 72;	'type' = 'ip-array';	'length' = 0; 'label' = "WWWServer" }
	'FingerServer' = @{					'code' = 73;	'type' = 'ip-array';	'length' = 0; 'label' = "FingerServer" }
	'IRCServer' = @{					'code' = 74;	'type' = 'ip-array';	'length' = 0; 'label' = "IRCServer" }
	'StreetTalkServer' = @{				'code' = 75;	'type' = 'ip-array';	'length' = 0; 'label' = "StreetTalkServer" }
	'STDAServer' = @{					'code' = 76;	'type' = 'ip-array';	'length' = 0; 'label' = "STDAServer" }
# RFC section 9
	'RequestedIPAddress' = @{			'code' = 50;	'type' = 'ip';			'length' = 4; 'label' = "RequestedIPAddress" }
	'IPAddressLeaseTime' = @{			'code' = 51;	'type' = 'int';			'length' = 4; 'label' = "IPAddressLeaseTime" }
	'OptionOverload' = @{				'code' = 52;	'type' = 'enum';		'length' = 1; 'label' = "OptionOverload" }
	'DHCPMessageType' = @{				'code' = 53;	'type' = 'enum';		'length' = 1; 'label' = "DHCPMessageType" }
	'ServerIdentifier' = @{				'code' = 54;	'type' = 'ip';			'length' = 4; 'label' = "ServerIdentifier" }
	'ParameterRequestList' = @{			'code' = 55;	'type' = 'array';		'length' = 0; 'label' = "ParameterRequestList" }
	'Message' = @{						'code' = 56;	'type' = 'string';		'length' = 0; 'label' = "Message" }
	'MaximumDHCPMessageSize' = @{		'code' = 57;	'type' = 'int';			'length' = 2; 'label' = "MaximumDHCPMessageSize" }
	'RenewalTimeValue' = @{				'code' = 58;	'type' = 'int';			'length' = 4; 'label' = "RenewalTimeValue" }
	'RebindingTimeValue' = @{			'code' = 59;	'type' = 'int';			'length' = 4; 'label' = "RebindingTimeValue" }
	'Vendorclassidentifier' = @{		'code' = 60;	'type' = 'string';		'length' = 0; 'label' = "Vendorclassidentifier" }
	'ClientIdentifier' = @{				'code' = 61;	'type' = 'raw';			'length' = 0; 'label' = "ClientIdentifier" }
	'TFTPservername' = @{				'code' = 66;	'type' = 'string';		'length' = 0; 'label' = "TFTPservername" }
	'Bootfilename' = @{					'code' = 67;	'type' = 'string';		'length' = 0; 'label' = "Bootfilename" }
#
	'END' = @{							'code' = 255;	'type' = 'none';		'length' = 1; 'label' = "END" }
}

# DHCPv4 options listed in an array
# @see @url http://www.ietf.org/rfc/rfc2132.txt
$Global:aDHCPv4Options = @(
	@{ 'code' = 0;	'type' = 'string';		'length' = 1; 'label' = "PAD";	"value" = 0 }
# RFC section 3
	@{ 'code' = 1;	'type' = 'string';		'length' = 4; 'label' = "SubnetMask" }
	@{ 'code' = 2;	'type' = 'string';		'length' = 4; 'label' = "TimeOffset" }
	@{ 'code' = 3;	'type' = 'string';		'length' = 0; 'label' = "Router" }
	@{ 'code' = 4;	'type' = 'string';		'length' = 0; 'label' = "TimeServer" }
	@{ 'code' = 5;	'type' = 'string';		'length' = 0; 'label' = "NameServer" }
	@{ 'code' = 6;	'type' = 'string';		'length' = 0; 'label' = "DomainNameServer" }
	@{ 'code' = 7;	'type' = 'string';		'length' = 0; 'label' = "LogServer" }
	@{ 'code' = 8;	'type' = 'string';		'length' = 0; 'label' = "CookieServer" }
	@{ 'code' = 9;	'type' = 'string';		'length' = 0; 'label' = "LPRServer" }
	@{ 'code' = 10;	'type' = 'string';		'length' = 0; 'label' = "ImpressServer" }
	@{ 'code' = 11;	'type' = 'string';		'length' = 0; 'label' = "ResourceLocationServer" }
	@{ 'code' = 12;	'type' = 'string';		'length' = 0; 'label' = "Hostname" }
	@{ 'code' = 13;	'type' = 'string';		'length' = 2; 'label' = "BootFileSize" }
	@{ 'code' = 14;	'type' = 'string';		'length' = 0; 'label' = "MeritDump" }
	@{ 'code' = 15;	'type' = 'string';		'length' = 0; 'label' = "DomainName" }
	@{ 'code' = 16;	'type' = 'string';		'length' = 0; 'label' = "SwapServer" }
	@{ 'code' = 17;	'type' = 'string';		'length' = 0; 'label' = "RootPath" }
	@{ 'code' = 18;	'type' = 'string';		'length' = 0; 'label' = "ExtensionsPath" }
# RFC section 4
	@{ 'code' = 19;	'type' = 'string';		'length' = 1; 'label' = "IPForwarding" }
	@{ 'code' = 20;	'type' = 'string';		'length' = 1; 'label' = "NonLocalSourceRouting" }
	@{ 'code' = 21;	'type' = 'string';		'length' = 0; 'label' = "PolicyFilter" }
	@{ 'code' = 22;	'type' = 'string';		'length' = 2; 'label' = "MaximumDatagramReassemblySize" }
	@{ 'code' = 23;	'type' = 'string';		'length' = 1; 'label' = "DefaultIPTTL" }
	@{ 'code' = 24;	'type' = 'string';		'length' = 4; 'label' = "PathMTUAgingTimeout" }
	@{ 'code' = 25;	'type' = 'string';		'length' = 0; 'label' = "PathMTUPlateauTable" }
# RFC section 5
	@{ 'code' = 26;	'type' = 'string';		'length' = 2; 'label' = "InterfaceMTU" }
	@{ 'code' = 27;	'type' = 'string';		'length' = 1; 'label' = "AllSubnetsareLocal" }
	@{ 'code' = 28;	'type' = 'string';		'length' = 4; 'label' = "BroadcastAddress" }
	@{ 'code' = 29;	'type' = 'string';		'length' = 1; 'label' = "PerformMaskDiscovery" }
	@{ 'code' = 30;	'type' = 'string';		'length' = 1; 'label' = "MaskSupplier" }
	@{ 'code' = 31;	'type' = 'string';		'length' = 1; 'label' = "PerformRouteriscovery" }
	@{ 'code' = 32;	'type' = 'string';		'length' = 4; 'label' = "RouterSolicitationddress" }
	@{ 'code' = 33;	'type' = 'string';		'length' = 0; 'label' = "StaticRoute" }
# RFC section 6
	@{ 'code' = 34;	'type' = 'string';		'length' = 1; 'label' = "TrailerEncapsulation" }
	@{ 'code' = 35;	'type' = 'string';		'length' = 4; 'label' = "ARPCacheimeout" }
	@{ 'code' = 36;	'type' = 'string';		'length' = 1; 'label' = "EthernetEncapsulation" }
# RFC section 7
	@{ 'code' = 37;	'type' = 'string';		'length' = 1; 'label' = "TCPDefaultTL" }
	@{ 'code' = 38;	'type' = 'string';		'length' = 4; 'label' = "TCPKeepaliventerval" }
	@{ 'code' = 39;	'type' = 'string';		'length' = 1; 'label' = "TCPKeepalivearbage" }
# RFC section 8
	@{ 'code' = 40;	'type' = 'string';		'length' = 0; 'label' = "NISDomain" }
	@{ 'code' = 41;	'type' = 'string';		'length' = 0; 'label' = "NISServer" }
	@{ 'code' = 42;	'type' = 'string';		'length' = 0; 'label' = "NTPServer" }
	@{ 'code' = 43;	'type' = 'string';		'length' = 0; 'label' = "VendorSpecificInformation" }
	@{ 'code' = 44;	'type' = 'string';		'length' = 0; 'label' = "NetBIOSNameServer" }
	@{ 'code' = 45;	'type' = 'string';		'length' = 0; 'label' = "NetBIOSDatagramDistributionServer" }
	@{ 'code' = 46;	'type' = 'string';		'length' = 1; 'label' = "NetBIOSNodeType" }
	@{ 'code' = 47;	'type' = 'string';		'length' = 0; 'label' = "NetBIOSScope" }
	@{ 'code' = 48;	'type' = 'string';		'length' = 0; 'label' = "XFontServer" }
	@{ 'code' = 49;	'type' = 'string';		'length' = 0; 'label' = "XDisplayManager" }
	# RFC section 9
	@{ 'code' = 50;	'type' = 'string';		'length' = 4; 'label' = "RequestedIPAddress" }
	@{ 'code' = 51;	'type' = 'string';		'length' = 4; 'label' = "IPAddressLeaseTime" }
	@{ 'code' = 52;	'type' = 'string';		'length' = 1; 'label' = "OptionOverload" }
	@{ 'code' = 53;	'type' = 'string';		'length' = 1; 'label' = "DHCPMessageType" }
	@{ 'code' = 54;	'type' = 'string';		'length' = 4; 'label' = "ServerIdentifier" }
	@{ 'code' = 55;	'type' = 'string';		'length' = 0; 'label' = "ParameterRequestList" }
	@{ 'code' = 56;	'type' = 'string';		'length' = 0; 'label' = "Message" }
	@{ 'code' = 57;	'type' = 'string';		'length' = 2; 'label' = "MaximumDHCPMessageSize" }
	@{ 'code' = 58;	'type' = 'string';		'length' = 4; 'label' = "RenewalTimeValue" }
	@{ 'code' = 59;	'type' = 'string';		'length' = 4; 'label' = "RebindingTimeValue" }
	@{ 'code' = 60;	'type' = 'string';		'length' = 0; 'label' = "Vendorclassidentifier" }
	@{ 'code' = 61;	'type' = 'string';		'length' = 0; 'label' = "ClientIdentifier" }
	@{ 'code' = 62;	}
	@{ 'code' = 63;	}
	# RFC section 8
	@{ 'code' = 64;	'type' = 'string';		'length' = 0; 'label' = "NISPLUSDomain" }
	@{ 'code' = 65;	'type' = 'string';		'length' = 0; 'label' = "NISPLUSServer" }
	# RFC section 9
	@{ 'code' = 66;	'type' = 'string';		'length' = 0; 'label' = "TFTPservername" }
	@{ 'code' = 67;	'type' = 'string';		'length' = 0; 'label' = "Bootfilename" }
	# RFC section 8
	@{ 'code' = 68;	'type' = 'string';		'length' = 0; 'label' = "MobileIPHomeAgent" }
	@{ 'code' = 69;	'type' = 'string';		'length' = 0; 'label' = "SMTPServer" }
	# RFC section 9
	@{ 'code' = 70;	'type' = 'string';		'length' = 0; 'label' = "POP3Server" }
	@{ 'code' = 71;	'type' = 'string';		'length' = 0; 'label' = "NNTPServer" }
	@{ 'code' = 72;	'type' = 'string';		'length' = 0; 'label' = "WWWServer" }
	@{ 'code' = 73;	'type' = 'string';		'length' = 0; 'label' = "FingerServer" }
	@{ 'code' = 74;	'type' = 'string';		'length' = 0; 'label' = "IRCServer" }
	@{ 'code' = 75;	'type' = 'string';		'length' = 0; 'label' = "StreetTalkServer" }
	@{ 'code' = 76;	'type' = 'string';		'length' = 0; 'label' = "STDAServer" }
#
	# @{ 'code' = 255;	'type' = 'string';		'length' = 1; 'label' = "END" }
)

# # number of options
# # @see @url http://www.ietf.org/rfc/rfc2132.txt
# WARN this syntax does not work
# enum eDHCPv4Options {
# 	PAD = @{	'code' = 0;	'type' = 'string';		'length' = 1; 'label' = "PAD";	"value" = 0 }
# # RFC section 3
# 	SubnetMask = @{	'code' = 1;	'type' = 'string';		'length' = 4; 'label' = "SubnetMask" }
# 	TimeOffset = @{	'code' = 2;	'type' = 'string';		'length' = 1; 'label' = "TimeOffset" }
# 	Router = @{	'code' = 3;	'type' = 'string';		'length' = 1; 'label' = "Router" }
# 	TimeServer = @{	'code' = 4;	'type' = 'string';		'length' = 1; 'label' = "TimeServer" }
# 	NameServer = @{	'code' = 5;	'type' = 'string';		'length' = 1; 'label' = "NameServer" }
# 	DomainNameServer = @{	'code' = 6;	'type' = 'string';		'length' = 1; 'label' = "DomainNameServer" }
# 	LogServer = @{	'code' = 7;	'type' = 'string';		'length' = 1; 'label' = "LogServer" }
# 	CookieServer = @{	'code' = 8;	'type' = 'string';		'length' = 1; 'label' = "CookieServer" }
# 	LPRServer = @{	'code' = 9;	'type' = 'string';		'length' = 1; 'label' = "LPRServer" }
# 	ImpressServer = @{	'code' = 10;	'type' = 'string';		'length' = 1; 'label' = "ImpressServer" }
# 	ResourceLocationServer = @{	'code' = 11;	'type' = 'string';		'length' = 1; 'label' = "ResourceLocationServer" }
# 	Hostname = @{	'code' = 12;	'type' = 'string';		'length' = 1; 'label' = "Hostname" }
# 	BootFileSize = @{	'code' = 13;	'type' = 'string';		'length' = 1; 'label' = "BootFileSize" }
# 	MeritDump = @{	'code' = 14;	'type' = 'string';		'length' = 1; 'label' = "MeritDump" }
# 	DomainName = @{	'code' = 15;	'type' = 'string';		'length' = 1; 'label' = "DomainName" }
# 	SwapServer = @{	'code' = 16;	'type' = 'string';		'length' = 1; 'label' = "SwapServer" }
# 	RootPath = @{	'code' = 17;	'type' = 'string';		'length' = 1; 'label' = "RootPath" }
# 	ExtensionsPath = @{	'code' = 18;	'type' = 'string';		'length' = 1; 'label' = "ExtensionsPath" }
# # RFC section 4
# 	IPForwarding = @{	'code' = 19;	'type' = 'string';		'length' = 1; 'label' = "IPForwarding" }
# 	NonLocalSourceRouting = @{	'code' = 20;	'type' = 'string';		'length' = 1; 'label' = "NonLocalSourceRouting" }
# 	PolicyFilter = @{	'code' = 21;	'type' = 'string';		'length' = 1; 'label' = "PolicyFilter" }
# 	MaximumDatagramReassemblySize = @{	'code' = 22;	'type' = 'string';		'length' = 1; 'label' = "MaximumDatagramReassemblySize" }
# 	DefaultIPTTL = @{	'code' = 23;	'type' = 'string';		'length' = 1; 'label' = "DefaultIPTTL" }
# 	PathMTUAgingTimeout = @{	'code' = 24;	'type' = 'string';		'length' = 1; 'label' = "PathMTUAgingTimeout" }
# 	PathMTUPlateauTable = @{	'code' = 25;	'type' = 'string';		'length' = 1; 'label' = "PathMTUPlateauTable" }
# # RFC section 5
# 	InterfaceMTU = @{	'code' = 26;	'type' = 'string';		'length' = 1; 'label' = "InterfaceMTU" }
# 	AllSubnetsareLocal = @{	'code' = 27;	'type' = 'string';		'length' = 1; 'label' = "AllSubnetsareLocal" }
# 	BroadcastAddress = @{	'code' = 28;	'type' = 'string';		'length' = 1; 'label' = "BroadcastAddress" }
# 	PerformMaskDiscovery = @{	'code' = 29;	'type' = 'string';		'length' = 1; 'label' = "PerformMaskDiscovery" }
# 	MaskSupplier = @{	'code' = 30;	'type' = 'string';		'length' = 1; 'label' = "MaskSupplier" }
# 	PerformRouteriscovery = @{	'code' = 31;	'type' = 'string';		'length' = 1; 'label' = "PerformRouteriscovery" }
# 	RouterSolicitationddress = @{	'code' = 32;	'type' = 'string';		'length' = 1; 'label' = "RouterSolicitationddress" }
# 	StaticRoute = @{	'code' = 33;	'type' = 'string';		'length' = 1; 'label' = "StaticRoute" }
# # RFC section 6
# 	TrailerEncapsulation = @{	'code' = 34;	'type' = 'string';		'length' = 1; 'label' = "TrailerEncapsulation" }
# 	ARPCacheimeout = @{	'code' = 35;	'type' = 'string';		'length' = 1; 'label' = "ARPCacheimeout" }
# 	EthernetEncapsulation = @{	'code' = 36;	'type' = 'string';		'length' = 1; 'label' = "EthernetEncapsulation" }
# # RFC section 7
# 	TCPDefaultTL = @{	'code' = 37;	'type' = 'string';		'length' = 1; 'label' = "TCPDefaultTL" }
# 	TCPKeepaliventerval = @{	'code' = 38;	'type' = 'string';		'length' = 1; 'label' = "TCPKeepaliventerval" }
# 	TCPKeepalivearbage = @{	'code' = 39;	'type' = 'string';		'length' = 1; 'label' = "TCPKeepalivearbage" }
# # RFC section 8
# 	NISDomain = @{	'code' = 40;	'type' = 'string';		'length' = 1; 'label' = "NISDomain" }
# 	NISServer = @{	'code' = 41;	'type' = 'string';		'length' = 1; 'label' = "NISServer" }
# 	NTPServer = @{	'code' = 42;	'type' = 'string';		'length' = 1; 'label' = "NTPServer" }
# 	VendorSpecificInformation = @{	'code' = 43;	'type' = 'string';		'length' = 1; 'label' = "VendorSpecificInformation" }
# 	NetBIOSNameServer = @{	'code' = 44;	'type' = 'string';		'length' = 1; 'label' = "NetBIOSNameServer" }
# 	NetBIOSDatagramDistributionServer = @{	'code' = 45;	'type' = 'string';		'length' = 1; 'label' = "NetBIOSDatagramDistributionServer" }
# 	NetBIOSNodeType = @{	'code' = 46;	'type' = 'string';		'length' = 1; 'label' = "NetBIOSNodeType" }
# 	NetBIOSScope = @{	'code' = 47;	'type' = 'string';		'length' = 1; 'label' = "NetBIOSScope" }
# 	XFontServer = @{	'code' = 48;	'type' = 'string';		'length' = 1; 'label' = "XFontServer" }
# 	XDisplayManager = @{	'code' = 49;	'type' = 'string';		'length' = 1; 'label' = "XDisplayManager" }
# 	NISPLUSDomain = @{	'code' = 64;	'type' = 'string';		'length' = 1; 'label' = "NISPLUSDomain" }
# 	NISPLUSServer = @{	'code' = 65;	'type' = 'string';		'length' = 1; 'label' = "NISPLUSServer" }
# 	MobileIPHomeAgent = @{	'code' = 68;	'type' = 'string';		'length' = 1; 'label' = "MobileIPHomeAgent" }
# 	SMTPServer = @{	'code' = 69;	'type' = 'string';		'length' = 1; 'label' = "SMTPServer" }
# 	POP3Server = 70
# 	NNTPServer = @{	'code' = 71;	'type' = 'string';		'length' = 1; 'label' = "NNTPServer" }
# 	WWWServer = @{	'code' = 72;	'type' = 'string';		'length' = 1; 'label' = "WWWServer" }
# 	FingerServer = @{	'code' = 73;	'type' = 'string';		'length' = 1; 'label' = "FingerServer" }
# 	IRCServer = @{	'code' = 74;	'type' = 'string';		'length' = 1; 'label' = "IRCServer" }
# 	StreetTalkServer = @{	'code' = 75;	'type' = 'string';		'length' = 1; 'label' = "StreetTalkServer" }
# 	STDAServer = @{	'code' = 76;	'type' = 'string';		'length' = 1; 'label' = "STDAServer" }
# # RFC section 9
# 	RequestedIPAddress = @{	'code' = 50;	'type' = 'string';		'length' = 1; 'label' = "RequestedIPAddress" }
# 	IPAddressLeaseTime = @{	'code' = 51;	'type' = 'string';		'length' = 1; 'label' = "IPAddressLeaseTime" }
# 	OptionOverload = @{	'code' = 52;	'type' = 'string';		'length' = 1; 'label' = "OptionOverload" }
# 	DHCPMessageType = @{	'code' = 53;	'type' = 'string';		'length' = 1; 'label' = "DHCPMessageType" }
# 	ServerIdentifier = @{	'code' = 54;	'type' = 'string';		'length' = 1; 'label' = "ServerIdentifier" }
# 	ParameterRequestList = @{	'code' = 55;	'type' = 'string';		'length' = 1; 'label' = "ParameterRequestList" }
# 	Message = @{	'code' = 56;	'type' = 'string';		'length' = 1; 'label' = "Message" }
# 	MaximumDHCPMessageSize = @{	'code' = 57;	'type' = 'string';		'length' = 1; 'label' = "MaximumDHCPMessageSize" }
# 	RenewalTimeValue = @{	'code' = 58;	'type' = 'string';		'length' = 1; 'label' = "RenewalTimeValue" }
# 	RebindingTimeValue = @{	'code' = 59;	'type' = 'string';		'length' = 1; 'label' = "RebindingTimeValue" }
# 	Vendorclassidentifier = @{	'code' = 60;	'type' = 'string';		'length' = 1; 'label' = "Vendorclassidentifier" }
# 	ClientIdentifier = @{	'code' = 61;	'type' = 'string';		'length' = 1; 'label' = "ClientIdentifier" }
# 	TFTPservername = @{	'code' = 66;	'type' = 'string';		'length' = 1; 'label' = "TFTPservername" }
# 	Bootfilename = @{	'code' = 67;	'type' = 'string';		'length' = 1; 'label' = "Bootfilename" }
# #
# 	END = @{	'code' = 255;	'type' = 'string';		'length' = 1; 'label' = "END" }
# }
