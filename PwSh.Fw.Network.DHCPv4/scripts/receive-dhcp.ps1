<#

	.SYNOPSIS
	Skeleton script for Klonebot-v4

	.DESCRIPTION
	Skeleton script for Klonebot-v4

	.PARAMETER h
	display help screen. Use Get-Help instead.

	.NOTES
	Author: Charles-Antoine Degennes <cadegenn@univ-lr.fr>

	.LINK
		https://gitlab.univ-lr.fr/klonebot-v4
#>

[CmdletBinding()]Param(
	[switch]$h = $false,
	[switch]$v = $false,
	[switch]$d = $false,
	[switch]$dev = $false,
	[switch]$trace = $false,
	[switch]$ask = $false,
	[Alias('ipAddress')]
	[Parameter(Mandatory = $false, ValueFromPipeLine = $false)][ipAddress]$RecvFrom = [net.ipaddress]::any,
	[Parameter(Mandatory = $false, ValueFromPipeLine = $false)][uint16]$port = 67,
	[uint16]$retries = 5,
	[uint16]$timeout = 1000,
	[switch]$Force = $false
)

$Global:BASENAME = Split-Path -Leaf $MyInvocation.MyCommand.Definition
Import-Module PwSh.Fw.Core -ErrorAction Stop -DisableNameChecking -Force:$Force
Set-PwShFwConfiguration -v:$v -d:$d -dev:$dev -trace:$trace -ask:$ask -quiet:$quiet

#############################
## YOUR SCRIPT BEGINS HERE ##
#############################
import-module -FullyQualifiedName $PSScriptRoot/../PwSh.Fw.Network.DHCPv4/PwSh.Fw.Network.DHCPv4.psd1 -Force:$Force

Receive-DHCPv4Packet -RecvFrom $RecvFrom -port $port -retries $retries -timeout $timeout

#############################
## YOUR SCRIPT ENDS   HERE ##
#############################

if ($log) {
	if ($TRACE) {
		Stop-Transcript
	} else {
		Write-ToLogFile -Message "------------------------------------------"
	}
}

# reinit values
$Global:DebugPreference = "SilentlyContinue"
Set-PSDebug -Off
$Script:indent = ""
