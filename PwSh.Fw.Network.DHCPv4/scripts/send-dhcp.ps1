<#
.SYNOPSIS
	Sample script to send a DHCP packet over the network
.DESCRIPTION
	Sample script to demonstrate the use of the DHCPv4 powershell class
.PARAMETER macAddress
	Mandatory.
	The mac address to ask for a DHCP lease.
.EXAMPLE
	PS C:\> ./send-dhcp.ps1 -macAddress "aa:bb:cc:dd:ee:ff"
	Send a DHCP discovery packet to the broadcast address requesting a response for the mac address "aa:bb:cc:dd:ee:ff"
#>
[CmdletBinding()]Param(
	[switch]$h = $false,
	[switch]$v = $false,
	[switch]$d = $false,
	[switch]$dev = $false,
	[switch]$trace = $false,
	[switch]$ask = $false,
	[Alias('ipAddress')]
	[ipAddress]$SendTo = [net.ipaddress]::Broadcast,
	[uint16]$port = 67,
	[Parameter(Mandatory = $true)][string]$macAddress,
	[uint16]$retries = 5,
	[uint16]$timeout = 1000,
	[switch]$Force = $false
)

$Global:BASENAME = Split-Path -Leaf $MyInvocation.MyCommand.Definition
Import-Module PwSh.Fw.Core -ErrorAction Stop -DisableNameChecking -Force:$Force
Set-PwShFwConfiguration -v:$v -d:$d -dev:$dev -trace:$trace -ask:$ask -quiet:$quiet

#############################
## YOUR SCRIPT BEGINS HERE ##
#############################
import-module -FullyQualifiedName $PSScriptRoot/../PwSh.Fw.Network.DHCPv4/PwSh.Fw.Network.DHCPv4.psd1 -Force:$Force

$dhcp = @{
	op = [DHCPv4MessageType]::DHCPDISCOVER	# @url http://www.ietf.org/rfc/rfc1700.txt [Page 163] 0x01 = BOOTREQUEST
	# op = [DHCPv4MessageType]::DHCPINFORM	# @url http://www.ietf.org/rfc/rfc1700.txt [Page 163] 0x01 = BOOTREQUEST
	htype = 1								# @url http://www.ietf.org/rfc/rfc1700.txt [Page 163]
	hlen = 6								# hardware address length = 6 bytes
	hops = 0								# Client sets to zero, optionally used by relay agents when booting via a relay agent.
	xid  = [DHCPv4Packet]::GenerateXID()	# transaction ID. ;"0x3903F326"
	secs = 0								# Filled in by client, seconds elapsed since client began address acquisition or renewal process.
	flags = 0								# @url http://www.ietf.org/rfc/rfc2131.txt [Page 11] : The leftmost bit is defined as the BROADCAST (B) flag.  The remaining bits of the flags field are reserved for future use.  They MUST be set to zero by clients and ignored by servers and relay agents.
	ciaddr = "0.0.0.0"
	yiaddr = "0.0.0.0"
	siaddr = "0.0.0.0"
	giaddr = "0.0.0.0"
	chaddr = $macAddress
	sname = ""
	file = ""
}

$dhcpDiscover = [DHCPv4Packet]::New($dhcp)
$dhcpDiscover.AddOption([int][DHCPv4OptionCode]::DHCPMessageType, [int][DHCPv4MessageType]::DHCPDISCOVER)
$dhcpDiscover.AddOption([int][DHCPv4OptionCode]::MaximumDHCPMessageSize, 1500)
$dhcpDiscover.AddRequestList([int][DHCPv4OptionCode]::SubnetMask)
$dhcpDiscover.AddRequestList([int][DHCPv4OptionCode]::Hostname)
$dhcpDiscover.AddRequestList([int][DHCPv4OptionCode]::DomainName)
$dhcpDiscover.AddRequestList([int][DHCPv4OptionCode]::SMTPServer)
$dhcpDiscover.AddRequestList([int][DHCPv4OptionCode]::NTPServer)
$dhcpDiscover

Send-DHCPv4Packet -SendTo $SendTo -port $port -packet $dhcpDiscover

#############################
## YOUR SCRIPT ENDS   HERE ##
#############################

if ($log) {
	if ($TRACE) {
		Stop-Transcript
	} else {
		Write-ToLogFile -Message "------------------------------------------"
	}
}

# reinit values
$Global:DebugPreference = "SilentlyContinue"
Set-PSDebug -Off
$Script:indent = ""
